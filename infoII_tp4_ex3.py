class File:
    def __init__(self):
        self.items = []

    def enqueue(self, item):
        # `item` est un tuple (priorité, donnée)
        # On insère l'élément à la bonne place en fonction de sa priorité
        if self.is_empty():
            self.items.append(item)
        else:
            # Chercher la position où insérer en fonction de la priorité
            inseré = False
            for i in range(len(self.items)):
                if item[0] < self.items[i][0]:  # Comparer les priorités
                    self.items.insert(i, item)
                    inseré = True
                    break
            if not inseré:
                self.items.append(item)

    def dequeue(self):
        return self.items.pop()

    def is_empty(self):
        return len(self.items) == 0

    def size(self):
        return len(self.items)

    def afficher_file(self):
        # Méthode utile pour afficher le contenu de la file
        return self.items


# Exemple d'utilisation
file_prioritaire = File()
file_prioritaire.enqueue((4, "Nathan"))
file_prioritaire.enqueue((2, "Julia"))
file_prioritaire.enqueue((1, "Amandine"))
file_prioritaire.enqueue((3, "Mathias"))

print("File après les insertions (par priorité croissante) :", file_prioritaire.afficher_file())

# Défilement (dequeue) des éléments de la plus haute priorité (1) à la plus basse
print("Défilement :", file_prioritaire.dequeue())  # Amandine (priorité 1)
print("Défilement :", file_prioritaire.dequeue())  # Julia (priorité 2)
print("Défilement :", file_prioritaire.dequeue())  # Mathias (priorité 3)
print("Défilement :", file_prioritaire.dequeue())  # Nathan (priorité 4)
